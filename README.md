Primeiro, suba o Docker:
```bash
docker-compose up
```

Depois, execute o cURL:
```bash
curl -X POST -H 'content-type: application/json' -d '{"productName":"XPTO", "qty":2, "unitPrice":100}' http://localhost:8888
```

Resultado esperado:
```bash
{"productName":"teslte","totalPrice":"196.00"}
```

Testes unitários:
```bash
composer test
```

Após fazer o push na Master, pode-se acompanhar o pipeline no Gitlab:
https://gitlab.com/thiagohsgoncalves/teste-pravaler/pipelines