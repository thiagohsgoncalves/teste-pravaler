<?php

declare(strict_types=1);

namespace Tests\Application\Actions\PraValer;

use App\Application\Actions\PraValerAction;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;
use Tests\TestCase;

class PraValerActionTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnTrueWhenPropertyIsNotEmpty()
    {
        $app = $this->getAppInstance();
        $container = $app->getContainer();
        $praValerAction = new PraValerAction($container);

        $praValerAction->setQty(10);
        $this->assertEquals($praValerAction->qty, 10);

        $praValerAction->setUnitPrice(22.50);
        $this->assertEquals($praValerAction->unitPrice, 22.50);

        $praValerAction->setProductName('XPTO');
        $this->assertEquals($praValerAction->productName, 'XPTO');
    }

    /**
     * @test
     */
    public function shouldApplyDiscountAndReturnFinalPrice()
    {
        $app = $this->getAppInstance();
        $container = $app->getContainer();
        $praValerAction = new PraValerAction($container);

        $praValerAction->setQty(1);
        $praValerAction->setUnitPrice(100);
        $praValerAction->setProductName('XPTO');

        $totalPrice = $praValerAction->calculateTotalPrice();

        $this->assertEquals($totalPrice, '98.00');
    }
}
