<?php

declare(strict_types=1);

namespace App\Application\Actions;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

class PraValerAction
{
    protected $request;
    public $response;
    protected $args;
    protected $container;
    protected $logger;
    public $productName;
    public $unitPrice;
    public $totalPrice;
    public $qty;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = $container->get(LoggerInterface::class);
    }

    public function __invoke(Request $request, Response $response, $args): Response
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        try {
            $body = $request->getParsedBody();
            return $this->process($body);
        } catch (\HttpBadRequestException $e) {
            $response->getBody()->write($e->getMessage());
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(500);
        }
    }

    public function process($body)
    {
        $this->setQty($body['qty']);
        $this->setProductName($body['productName']);
        $this->setUnitPrice($body['unitPrice']);
        $this->totalPrice = $this->calculateTotalPrice();
        return $this->responseWithSuccess();
    }

    public function responseWithSuccess(): Response
    {
        $payload = json_encode([
            'productName' => $this->productName,
            'totalPrice' => $this->totalPrice
        ]);

        $this->response->getBody()->write($payload);

        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }

    public function setUnitPrice(float $unitPrice): void
    {
        $this->unitPrice = filter_var($unitPrice, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    }

    public function setQty(int $qty): void
    {
        if ($qty === 0) {
            $this->logger->debug("Invalid param, qty must be gt 0. Received: {$qty}");
            throw new HttpBadRequestException($this->request, "A quantidade deve ser maior que zero");
        }
        $this->qty = filter_var($qty, FILTER_VALIDATE_INT);
    }

    public function setProductName(string $productName): void
    {
        $this->productName = filter_var($productName, FILTER_SANITIZE_STRING);
    }

    public function calculateTotalPrice(): string
    {
        $discount = 2;
        if ($this->qty > 5 && $this->qty <= 10) {
            $discount = 3;
        } elseif ($this->qty > 10) {
            $discount = 5;
        }

        $totalPrice = $this->qty * $this->unitPrice;
        $totalPrice -= ($discount / 100) * $totalPrice;
        return number_format($totalPrice, 2);
    }
}
